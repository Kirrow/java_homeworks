package homeworks_module_01.Homework_08_happy_family_5;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class FamilyServiceTest {
    FamilyDao familyDao;
    FamilyService familyService;
    Human father1;
    Human mother1;
    Family family1;
    Human father2;
    Human mother2;
    Family family2;



    @Before
    public void setUp() throws Exception {
        father1 = new Man("Ivan", "Ivanov", 1980);
        mother1 = new Woman("Masha", "Ivanova", 1985);
        family1 = new Family(mother1, father1);
        father2 = new Man("Vova", "Petrov", 1990);
        mother2 = new Woman("Elena", "Petrova", 1995);
        family2 = new Family(mother2, father2);
        familyDao = new CollectionFamilyDao();
        familyService = new FamilyService(familyDao);
        familyService.createNewFamily(father1, mother1);
        familyService.createNewFamily(father2, mother2);
    }

    @Test
    public void getAllFamilies() {
        List<Family> expected = familyService.getAllFamilies();

        List<Family> actual = new ArrayList<>();
        actual.add(family1);
        actual.add(family2);
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void displayAllFamilies() {
        String expected = "[Family{mother=Human{name='Masha', surname='Ivanova', year=1985, iq=0, schedule=null}, father=Human{name='Ivan', surname='Ivanov', year=1980, iq=0, schedule=null}, children=[], pet=[]}, Family{mother=Human{name='Elena', surname='Petrova', year=1995, iq=0, schedule=null}, father=Human{name='Vova', surname='Petrov', year=1990, iq=0, schedule=null}, children=[], pet=[]}]";

        List<Family> actual = new ArrayList<>();
        actual.add(family1);
        actual.add(family2);
        assertEquals(expected, actual.toString());

    }

    @Test
    public void getFamiliesBiggerThan() {
        List<Family> expected = familyService.getFamiliesBiggerThan(1);

        List<Family> actual = new ArrayList<>();
        actual.add(family1);
        actual.add(family2);
        assertEquals(expected, actual);
    }

    @Test
    public void getFamiliesLessThan() {
        List<Family> expected = familyService.getFamiliesLessThan(2);

        List<Family> actual = new ArrayList<>();
        assertEquals(expected, actual);
    }

    @Test
    public void countFamiliesWithMemberNumber() {
        int expected = familyService.countFamiliesWithMemberNumber(2);

        assertEquals(expected, 2);
    }

    @Test
    public void createNewFamily() {
        familyService.createNewFamily(father1, mother1);
        Family family = familyService.getFamilyById(0);
        assertEquals(family, family1);
    }

    @Test
    public void deleteFamilyByIndex() {
        familyService.deleteFamilyByIndex(0);
        List<Family> remainingFamilies = familyService.getAllFamilies();
        assertEquals(remainingFamilies, List.of(family2));
    }

    @Test
    public void bornChild() {
        List<Human> countOfChildrenBefore = familyService.getFamilyById(0).getChildren();
        assertEquals(countOfChildrenBefore.size(), 0);

        familyService.bornChild(family1, "Nick", "Rita");

        List<Human> countOfChildrenAfterAdd = familyService.getFamilyById(0).getChildren();
        assertEquals(countOfChildrenAfterAdd.size(), 1);
    }

    @Test
    public void adoptChild() {
        Woman childAnna = new Woman("Anna", "Ivanova", 0);
        List<Human> countOfChildrenBeforeAdopt = familyService.getFamilyById(0).getChildren();
        assertEquals(countOfChildrenBeforeAdopt.size(), 0);

        familyService.adoptChild(family1, childAnna);

        List<Human> countOfChildrenAfterAdopt = familyService.getFamilyById(0).getChildren();
        assertEquals(countOfChildrenAfterAdopt.size(), 1);
    }

    @Test
    public void deleteAllChildrenOlderThen() {
        Woman childAnna = new Woman("Anna", "Ivanova", 10);
        Woman childAlice = new Woman("Alice", "Ivanova", 17);

        familyService.adoptChild(family1, childAlice);
        familyService.adoptChild(family1, childAnna);

        familyService.deleteAllChildrenOlderThen(14);
        List<Human> countOfChildrenAfterDelete = familyService.getFamilyById(0).getChildren();
        assertEquals(countOfChildrenAfterDelete.size(), 1);

    }

    @Test
    public void count() {
        int expectedCount = familyService.count();

        assertEquals(expectedCount, 2);
    }

    @Test
    public void getFamilyById() {
        assertEquals(family1, familyService.getFamilyById(0));
    }

    @Test
    public void getPets() {
    }

    @Test
    public void addPet() {
        Dog dog = new Dog("Мушу", 5, 60, new HashSet<>());
        assertEquals(familyService.getPets(0).size(), 0);
        familyService.addPet(0, dog);
        Set<Pet> result = familyService.getPets(0);
        assertEquals(result.size(), 1);

    }
}