package homeworks_module_01.homeworks.homework05;

import homeworks_module_01.Homework_05_happy_family_2.DayOfWeek;
import homeworks_module_01.Homework_05_happy_family_2.Family;
import homeworks_module_01.Homework_05_happy_family_2.Human;
import junit.framework.TestSuite;
import org.junit.*;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple App.
 */
public class TestHomework5 {

      @Test
    public void testStringClassMethod_Good() {
          Human motherTestData = new Human("Lisa", "Ivanova", 2000, (byte) 90, new String[][]{{DayOfWeek.SATURDAY.name(), "Work"}});
          String stringData = motherTestData.toString();
          String expected = "Human{name='Lisa', surname='Ivanova', year=2000, iq=90, schedule=[[SATURDAY, Work]]}";

            assertEquals(stringData, expected);
    }
    @Test
    public void testStringClassMethod_Bad() {
        Human motherTestData = new Human("Lisa", "Ivanova", 2000, (byte) 90, new String[][]{{DayOfWeek.SATURDAY.name(), "Work"}});
        String stringData = motherTestData.toString();

        System.out.println("Result is: " + stringData);
    }
    @Test
    public void testDeleteChild_Good() {
        String[][] fatherTasks = {{DayOfWeek.MONDAY.name(), "Do homework"}, {DayOfWeek.THURSDAY.name(), "Go to pool"}};
        Human father = new Human("Ivan", "Ivanov", 1980, (byte) 80, fatherTasks);
        Human mother = new Human("Masha", "Ivanova", 1985);
        Human childAlice = new Human("Alice", "Ivanova", 2010, (byte) 90, new String[][]{{DayOfWeek.SATURDAY.name(), "Work"}});
        Human childAnna = new Human("Anna", "Ivanova", 2014, (byte) 85, new String[][]{{DayOfWeek.FRIDAY.name(), "Sleep"}});
        Family family = new Family(mother, father);
        family.addChild(childAlice);
        family.addChild(childAnna);

        int childrenNum = family.getChildren().length;
        Assert.assertEquals(2, childrenNum);

        boolean result = family.deleteChild(childAnna);
        Assert.assertEquals(true, Boolean.valueOf(result));
        Assert.assertEquals(1, family.getChildren().length);

    }
    @Test
    public void testDeleteChild_Bad() {
        String[][] fatherTasks = {{DayOfWeek.MONDAY.name(), "Do homework"}, {DayOfWeek.THURSDAY.name(), "Go to pool"}};
        Human father = new Human("Ivan", "Ivanov", 1980, (byte) 80, fatherTasks);
        Human mother = new Human("Masha", "Ivanova", 1985);
        Human childAlice = new Human("Alice", "Ivanova", 2010, (byte) 90, new String[][]{{DayOfWeek.SATURDAY.name(), "Work"}});
        Human childAnna = new Human("Anna", "Ivanova", 2014, (byte) 85, new String[][]{{DayOfWeek.FRIDAY.name(), "Sleep"}});
        Family family = new Family(mother, father);
        family.addChild(childAlice);
        family.addChild(childAnna);

        Human childOther = new Human("Dasha", "Petrova", 2015);

        System.out.println("Count of Children were: " + family.getChildren().length);

        boolean result = family.deleteChild(childOther);
        System.out.println("Children in family: " + Arrays.deepToString(family.getChildren()));
        System.out.println("Count of Children remain: " + family.getChildren().length);
        System.out.println("Result of deleteChild method - " + result);
    }

    @Test
    public void testAddChild_Good() {
        String[][] fatherTasks = {{DayOfWeek.MONDAY.name(), "Do homework"}, {DayOfWeek.THURSDAY.name(), "Go to pool"}};
        Human father = new Human("Ivan", "Ivanov", 1980, (byte) 80, fatherTasks);
        Human mother = new Human("Masha", "Ivanova", 1985);
        Human childAlice = new Human("Alice", "Ivanova", 2010, (byte) 90, new String[][]{{DayOfWeek.SATURDAY.name(), "Work"}});
        Human childAnna = new Human("Anna", "Ivanova", 2014, (byte) 85, new String[][]{{DayOfWeek.FRIDAY.name(), "Sleep"}});
        Family family = new Family(mother, father);
        family.addChild(childAlice);
        family.addChild(childAnna);

        Human childOther = new Human("Dasha", "Petrova", 2015);

        family.addChild(childOther);
        Assert.assertEquals(3, family.getChildren().length);
        Assert.assertEquals(childOther, family.getChildren()[2]);
    }

    @Test
    public void testAddChild_Bad() {
        String[][] fatherTasks = {{DayOfWeek.MONDAY.name(), "Do homework"}, {DayOfWeek.THURSDAY.name(), "Go to pool"}};
        Human father = new Human("Ivan", "Ivanov", 1980, (byte) 80, fatherTasks);
        Human mother = new Human("Masha", "Ivanova", 1985);
        Human childAlice = new Human("Alice", "Ivanova", 2010, (byte) 90, new String[][]{{DayOfWeek.SATURDAY.name(), "Work"}});
        Human childAnna = new Human("Anna", "Ivanova", 2014, (byte) 85, new String[][]{{DayOfWeek.FRIDAY.name(), "Sleep"}});
        Family family = new Family(mother, father);
        family.addChild(childAlice);
        family.addChild(childAnna);

        Human childOther = new Human("Dasha", "Petrova", 2015);

        family.addChild(childOther);
        System.out.println("Number of children: " + family.getChildren().length);
    }

    @Test
    public void testCountFamily_Good() {
        String[][] fatherTasks = {{DayOfWeek.MONDAY.name(), "Do homework"}, {DayOfWeek.THURSDAY.name(), "Go to pool"}};
        Human father = new Human("Ivan", "Ivanov", 1980, (byte) 80, fatherTasks);
        Human mother = new Human("Masha", "Ivanova", 1985);
        Human childAlice = new Human("Alice", "Ivanova", 2010, (byte) 90, new String[][]{{DayOfWeek.SATURDAY.name(), "Work"}});
        Human childAnna = new Human("Anna", "Ivanova", 2014, (byte) 85, new String[][]{{DayOfWeek.FRIDAY.name(), "Sleep"}});
        Family family = new Family(mother, father);
        family.addChild(childAlice);
        family.addChild(childAnna);

        Assert.assertEquals(4, family.countFamily());
    }

    @Test
    public void testCountFamily_Bad() {
        String[][] fatherTasks = {{DayOfWeek.MONDAY.name(), "Do homework"}, {DayOfWeek.THURSDAY.name(), "Go to pool"}};
        Human father = new Human("Ivan", "Ivanov", 1980, (byte) 80, fatherTasks);
        Human mother = new Human("Masha", "Ivanova", 1985);
        Human childAlice = new Human("Alice", "Ivanova", 2010, (byte) 90, new String[][]{{DayOfWeek.SATURDAY.name(), "Work"}});
        Human childAnna = new Human("Anna", "Ivanova", 2014, (byte) 85, new String[][]{{DayOfWeek.FRIDAY.name(), "Sleep"}});
        Family family = new Family(mother, father);
        family.addChild(childAlice);
        family.addChild(childAnna);

        System.out.println("Family consist of " + family.countFamily() + " people");
    }
}

