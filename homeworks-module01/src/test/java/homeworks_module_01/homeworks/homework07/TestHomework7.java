package homeworks_module_01.homeworks.homework07;

import homeworks_module_01.Homework_07_happy_family_4.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Unit test for simple App.
 */
public class TestHomework7 {

      @Test
    public void testStringClassMethod_Good() {
          Woman motherTestData = new Woman("Lisa", "Ivanova", 2000, (byte) 90, Map.of(DayOfWeek.SATURDAY.name(), "Work"));
          String stringData = motherTestData.toString();
          String expected = "Human{name='Lisa', surname='Ivanova', year=2000, iq=90, schedule={SATURDAY=Work}}";

            assertEquals(expected, stringData);
    }
    @Test
    public void testStringClassMethod_Bad() {
        Human motherTestData = new Human("Lisa", "Ivanova", 2000, (byte) 90, Map.of(DayOfWeek.SATURDAY.name(), "Work"));
        String stringData = motherTestData.toString();

        System.out.println("Result is: " + stringData);
    }
    @Test
    public void testDeleteChild_Good() {
        Map<String, String> fatherTasks = Map.of(DayOfWeek.MONDAY.name(), "Do homework", DayOfWeek.THURSDAY.name(), "Go to pool");
        Man father = new Man("Ivan", "Ivanov", 1980, (byte) 80, fatherTasks);
        Woman mother = new Woman("Masha", "Ivanova", 1985);
        Woman childAlice = new Woman("Alice", "Ivanova", 2010, (byte) 90, Map.of(DayOfWeek.SATURDAY.name(), "Work"));
        Woman childAnna = new Woman("Anna", "Ivanova", 2014, (byte) 85, Map.of(DayOfWeek.FRIDAY.name(), "Sleep"));
        Family family = new Family(mother, father);
        family.addChild(childAlice);
        family.addChild(childAnna);

        int childrenNum = family.getChildren().size();
        Assert.assertEquals(2, childrenNum);

        boolean result = family.deleteChild(childAnna);
        Assert.assertEquals(true, Boolean.valueOf(result));
        Assert.assertEquals(1, family.getChildren().size());

    }
    @Test
    public void testDeleteChild_Bad() {
        Map<String, String> fatherTasks = Map.of(DayOfWeek.MONDAY.name(), "Do homework", DayOfWeek.THURSDAY.name(), "Go to pool");
        Man father = new Man("Ivan", "Ivanov", 1980, (byte) 80, fatherTasks);
        Woman mother = new Woman("Masha", "Ivanova", 1985);
        Woman childAlice = new Woman("Alice", "Ivanova", 2010, (byte) 90, Map.of(DayOfWeek.SATURDAY.name(), "Work"));
        Woman childAnna = new Woman("Anna", "Ivanova", 2014, (byte) 85, Map.of(DayOfWeek.FRIDAY.name(), "Sleep"));
        Family family = new Family(mother, father);
        family.addChild(childAlice);
        family.addChild(childAnna);

        Woman childOther = new Woman("Dasha", "Petrova", 2015);

        System.out.println("Count of Children were: " + family.getChildren().size());

        boolean result = family.deleteChild(childOther);
        System.out.println("Children in family was: " + family.getChildren());
        System.out.println("Count of Children was: " + family.getChildren().size());
        System.out.println("Result of deleteChild method - " + result);
        System.out.println("Count of Children after: " + family.getChildren().size());
    }

    @Test
    public void testAddChild_Good() {
        Map<String, String> fatherTasks = Map.of(DayOfWeek.MONDAY.name(), "Do homework", DayOfWeek.THURSDAY.name(), "Go to pool");
        Man father = new Man("Ivan", "Ivanov", 1980, (byte) 80, fatherTasks);
        Woman mother = new Woman("Masha", "Ivanova", 1985);
        Woman childAlice = new Woman("Alice", "Ivanova", 2010, (byte) 90, Map.of(DayOfWeek.SATURDAY.name(), "Work"));
        Woman childAnna = new Woman("Anna", "Ivanova", 2014, (byte) 85, Map.of(DayOfWeek.FRIDAY.name(), "Sleep"));
        Family family = new Family(mother, father);
        family.addChild(childAlice);
        family.addChild(childAnna);

        Woman childOther = new Woman("Dasha", "Petrova", 2015);

        family.addChild(childOther);
        Assert.assertEquals(3, family.getChildren().size());
        Assert.assertEquals(childOther, family.getChildren().get(2));
    }

    @Test
    public void testAddChild_Bad() {
        Map<String, String> fatherTasks = Map.of(DayOfWeek.MONDAY.name(), "Do homework", DayOfWeek.THURSDAY.name(), "Go to pool");
        Man father = new Man("Ivan", "Ivanov", 1980, (byte) 80, fatherTasks);
        Woman mother = new Woman("Masha", "Ivanova", 1985);
        Woman childAlice = new Woman("Alice", "Ivanova", 2010, (byte) 90, Map.of(DayOfWeek.SATURDAY.name(), "Work"));
        Woman childAnna = new Woman("Anna", "Ivanova", 2014, (byte) 85, Map.of(DayOfWeek.FRIDAY.name(), "Sleep"));
        Family family = new Family(mother, father);
        family.addChild(childAlice);
        family.addChild(childAnna);

        Woman childOther = new Woman("Dasha", "Petrova", 2015);

        family.addChild(childOther);
        System.out.println("Number of children: " + family.getChildren().size());
    }

    @Test
    public void testCountFamily_Good() {
        Map<String, String> fatherTasks = Map.of(DayOfWeek.MONDAY.name(), "Do homework", DayOfWeek.THURSDAY.name(), "Go to pool");
        Man father = new Man("Ivan", "Ivanov", 1980, (byte) 80, fatherTasks);
        Woman mother = new Woman("Masha", "Ivanova", 1985);
        Woman childAlice = new Woman("Alice", "Ivanova", 2010, (byte) 90, Map.of(DayOfWeek.SATURDAY.name(), "Work"));
        Woman childAnna = new Woman("Anna", "Ivanova", 2014, (byte) 85, Map.of(DayOfWeek.FRIDAY.name(), "Sleep"));
        Family family = new Family(mother, father);
        family.addChild(childAlice);
        family.addChild(childAnna);

        Assert.assertEquals(4, family.countFamily());
    }

    @Test
    public void testCountFamily_Bad() {
        Map<String, String> fatherTasks = Map.of(DayOfWeek.MONDAY.name(), "Do homework", DayOfWeek.THURSDAY.name(), "Go to pool");
        Man father = new Man("Ivan", "Ivanov", 1980, (byte) 80, fatherTasks);
        Woman mother = new Woman("Masha", "Ivanova", 1985);
        Woman childAlice = new Woman("Alice", "Ivanova", 2010, (byte) 90, Map.of(DayOfWeek.SATURDAY.name(), "Work"));
        Woman childAnna = new Woman("Anna", "Ivanova", 2014, (byte) 85, Map.of(DayOfWeek.FRIDAY.name(), "Sleep"));
        Family family = new Family(mother, father);
        family.addChild(childAlice);
        family.addChild(childAnna);

        System.out.println("Family consist of " + family.countFamily() + " people");
    }
}

