package homeworks_module_01.Homework_11_happy_family_8;

public enum Species {
    FISH("fish"),
    DOG("dog"),
    DOMESTICCAT("domestic cat"),
    ROBOCAT("robocat"),
    UNKNOWN("unknown");

    public String pet;
    Species(String pet) {
        this.pet = pet;
    }
}


