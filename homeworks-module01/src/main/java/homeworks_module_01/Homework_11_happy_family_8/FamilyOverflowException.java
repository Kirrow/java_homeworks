package homeworks_module_01.Homework_11_happy_family_8;

public class FamilyOverflowException extends RuntimeException {
        public FamilyOverflowException() {
            super("Family size exceeds the limit.");
        }

        public FamilyOverflowException(String message) {
            super(message);
        }

}
