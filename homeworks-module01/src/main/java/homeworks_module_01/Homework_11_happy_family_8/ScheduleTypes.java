package homeworks_module_01.Homework_11_happy_family_8;

public enum ScheduleTypes {
    HOMEWORK("Do homework"),
    GYM("Go to gym"),
    WALK("Go to walk"),
    CINEMA("Go to cinema");

    String sceduleType;

    ScheduleTypes(String sceduleType) {
        this.sceduleType = sceduleType;
    }

}
