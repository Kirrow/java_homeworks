package homeworks_module_01.Homework_11_happy_family_8;

public class UserMenu {
    public void printMenu(){
        System.out.println("""
                1. Заповнити тестовими даними (автоматом створити кілька сімей та зберегти їх у базі)
                2. Відобразити весь список сімей (відображає список усіх сімей з індексацією, що починається з 1)
                3. Відобразити список сімей, де кількість людей більша за задану
                4. Відобразити список сімей, де кількість людей менша за задану
                5. Підрахувати кількість сімей, де кількість членів дорівнює
                6. Створити нову родину
                7. Видалити сім'ю за індексом сім'ї у загальному списку
                8. Редагувати сім'ю за індексом сім'ї у загальному списку
                9. Видалити всіх дітей старше віку (у всіх сім'ях видаляються діти старше зазначеного віку - вважатимемо, що вони виросли)
                0. Закінчити
                """);
    }

    public void printEditFamilySubMenu(){
        System.out.println("""
                   1. Народити дитину
                   2. Усиновити дитину
                   3. Повернутися до головного меню
                """);
    }
}
