package homeworks_module_01.Homework_02_area_shooting;

import java.util.Arrays;
import java.util.Scanner;

public class Homework02 {
    static int userColumnNumber;
    static int userRawNumber;
    static int targetColumn = (int) (Math.random() * 5);
    static int targetRaw = (int) (Math.random() * 5);
    static boolean isTargetHited = false;
    static Scanner in = new Scanner(System.in);
    static String[][] aimArray = new String[][]{
            {"0", "1", "2", "3", "4", "5"},
            {"1", "-", "-", "-", "-", "-"},
            {"2", "-", "-", "-", "-", "-"},
            {"3", "-", "-", "-", "-", "-"},
            {"4", "-", "-", "-", "-", "-"},
            {"5", "-", "-", "-", "-", "-"}
    };

    public static void main(String[] args) {

        printMessage("All Set. Get ready to rumble!", true);
//        System.out.println("target: column - " + targetColumn + "raw - " + targetRaw);

        printTarget();

        do {
            getNumberOfColumn();
            getNumberOfRaw();

            if(targetColumn == userColumnNumber && targetRaw == userRawNumber){

                aimArray[userRawNumber][userColumnNumber] = "X";
                printTarget();
                System.out.println("You have won!");
                isTargetHited = true;
            } else {
                aimArray[userRawNumber][userColumnNumber] = "*";
                printTarget();
            };

        } while (!isTargetHited);

    }

    public static void printMessage(String message, boolean isWrapToNextLine){
        System.out.print(message + (isWrapToNextLine ? "\n" : ""));
    };

    public static void getNumberOfColumn(){
        printMessage("Input number of column: ", false);
        String enteredNumberColumn = in.nextLine();

        try{
            userColumnNumber = Integer.parseInt(enteredNumberColumn.trim());
            if(userColumnNumber < 1 || userColumnNumber > 5){
                System.out.println("Your number should be more than 0 or not more than 5");
                getNumberOfColumn();
            };
        } catch (NumberFormatException nfe){
            getNumberOfColumn();
        }
    };

    public static void getNumberOfRaw(){

        printMessage("Input number of raw: ", false);
        String enteredNumberColumn = in.nextLine();

        try{
            userRawNumber = Integer.parseInt(enteredNumberColumn.trim());
            if(userRawNumber < 1 || userRawNumber > 5){
                System.out.println("Your number should be more than 0 or not more than 5");
                getNumberOfRaw();
            };
        } catch (NumberFormatException nfe){
            getNumberOfRaw();
        }

    };

    public static void printTarget(){
        for (String[] elemRaw: aimArray) {
            String str = String.join(" | ", elemRaw);
            String strFinal = str + " |";
            System.out.println(strFinal);
        }
    };
}
