package homeworks_module_01.Homework_09_happy_family_6;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    ArrayList<Family> families;

    public CollectionFamilyDao() {
        this.families = new ArrayList<>();
    }

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return families.get(index);
    }

    @Override
    public Boolean deleteFamily(int index) {
        if (index >= 0 && index < families.size()) {
            families.remove(index);
            return true;
        }
            return false;
    }

    @Override
    public Boolean deleteFamily(Family familyToDelete) {
        if (families.contains(familyToDelete)) {
            families.remove(familyToDelete);
            return true;
        }
            return false;
    }

    @Override
    public void saveFamily(Family family) {
        if(families.contains(family)){
            int indexOfFamily = families.indexOf(family);
            families.set(indexOfFamily, family);
        } else {
            families.add(family);
        }
    }
}
