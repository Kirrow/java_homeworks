package homeworks_module_01.Homework_09_happy_family_6;

import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    Boolean deleteFamily(int index);
    Boolean deleteFamily(Family family);
    void saveFamily(Family family);


}
