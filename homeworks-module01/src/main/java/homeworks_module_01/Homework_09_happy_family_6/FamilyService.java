package homeworks_module_01.Homework_09_happy_family_6;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {

    FamilyDao familyDao;
    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies(){
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies(){
        List<Family> families = getAllFamilies();
        for (int i = 0; i < families.size(); i++) {
            Family currentFamily = families.get(i);
            System.out.printf("Family # %d: %s\n", i + 1, currentFamily);
        }
    }

    public List<Family> getFamiliesBiggerThan(int familyBiggerThan){
        List<Family> allFamilies = getAllFamilies();
        List<Family> filteredFamilies = allFamilies.stream().filter(family -> family.countFamily() > familyBiggerThan).collect(Collectors.toList());
        return filteredFamilies;
    }

    public List<Family> getFamiliesLessThan(int familyLessThan){
        List<Family> allFamilies = getAllFamilies();
        List<Family> filteredFamilies = allFamilies.stream().filter(family -> family.countFamily() < familyLessThan).collect(Collectors.toList());
        return filteredFamilies;
    }

    public int countFamiliesWithMemberNumber(int countOfPeopleInFamily){
        List<Family> allFamilies = getAllFamilies();
        int filteredFamiliesByPeopleNumber = (int) allFamilies.stream().filter(family -> family.countFamily() == countOfPeopleInFamily).count();
        return filteredFamiliesByPeopleNumber;
    }

    public void createNewFamily(Human father, Human mother){
        Family newFamily = new Family(mother, father);
        familyDao.saveFamily(newFamily);
    }

    public void deleteFamilyByIndex(int indexOfFamily){
        try {
            familyDao.deleteFamily(indexOfFamily);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Family bornChild(Family family, String maleName, String femaleName){
        Human child = new Human();
        boolean isNewChildMan = Math.random() < 0.5;

        child.setName(isNewChildMan ? maleName : femaleName);
        child.setSurname(family.getFather().getSurname() + (isNewChildMan ? "" : "a"));
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human childToAdoption){
        family.addChild(childToAdoption);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age){
        List<Family> families = getAllFamilies();
        for (Family family :
                families) {
            family.getChildren().removeIf(child -> (child.getYear() > age));
            familyDao.saveFamily(family);
        }
    }

    public int count(){
        return getAllFamilies().size();
    }

    Family getFamilyById(int familyIndex){
        return familyDao.getFamilyByIndex(familyIndex);
    }

    Set<Pet> getPets(int familyIndex){
        return getFamilyById(familyIndex).getPets();
    }

    public void addPet(int familyIndex, Pet newPet){
        Family currentFamily = getFamilyById(familyIndex);
        currentFamily.setPet(newPet);
        familyDao.saveFamily(currentFamily);
    }

}
