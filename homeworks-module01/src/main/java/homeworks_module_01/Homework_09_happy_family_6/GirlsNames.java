package homeworks_module_01.Homework_09_happy_family_6;

public enum GirlsNames {
    MARY("Mary"),
    JULIYA("Juliya"),
    ANNA("Anna"),
    ELENA("Elena"),
    ZOYA("Zoya"),
    DARYA("Darya"),
    LANA("Lana"),
    GALYA("Galya");

    public String name;

    GirlsNames(String name){
        this.name = name;
    }

}
