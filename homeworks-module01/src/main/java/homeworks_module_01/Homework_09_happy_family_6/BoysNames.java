package homeworks_module_01.Homework_09_happy_family_6;

public enum BoysNames {
    LUCK("Luck"),
    JACK("Jack"),
    JOHN("John"),
    ROBERT("Robert"),
    JIM("Jim"),
    BOB("Bob");

    public String name;

    BoysNames(String name){
        this.name = name;
    }
}
