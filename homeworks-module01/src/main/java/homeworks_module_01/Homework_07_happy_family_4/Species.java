package homeworks_module_01.Homework_07_happy_family_4;

public enum Species {
    FISH("fish"),
    DOG("dog"),
    DOMESTICCAT("domestic cat"),
    ROBOCAT("robocat"),
    UNKNOWN("unknown");

    public String pet;
    Species(String pet) {
        this.pet = pet;
    }
}


