package homeworks_module_01.Homework_07_happy_family_4;

import java.util.Map;

public final class Man extends Human {

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, byte iq, Map<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPet(String typeOfPet) {
        Pet chosenPet = super.chosePetFromList(typeOfPet);
        System.out.printf("Привіт, %s \n", chosenPet.getNickname());
    }

    public void repairCar(){
        System.out.println("Відремонтував авто!");
    }
}
