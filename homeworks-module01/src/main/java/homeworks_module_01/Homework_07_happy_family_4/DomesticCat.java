package homeworks_module_01.Homework_07_happy_family_4;

import java.util.Set;

public class DomesticCat extends Pet implements Foul {
    public Species species = Species.DOMESTICCAT;

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);

    };
    public void respond(){
        System.out.printf("Привіт!. Я - %s. Що в нас на вечерю? \n", super.getNickname());
    };

}