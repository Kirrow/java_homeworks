package homeworks_module_01.Homework_07_happy_family_4;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        // ============= FAMILY 1 =====================
        Map<String, String> father1Tasks = Map.of(DayOfWeek.MONDAY.name(), "Do homework", DayOfWeek.THURSDAY.name(), "Go to pool");
        Man father1 = new Man("Ivan", "Ivanov", 1980, (byte) 80, father1Tasks);
        Woman mother1 = new Woman("Masha", "Ivanova", 1985);
        Woman childAlice = new Woman("Alice", "Ivanova", 2010, (byte) 90, Map.of(DayOfWeek.SATURDAY.name(), "Work"));
        Woman childAnna = new Woman("Anna", "Ivanova", 2014, (byte) 85, Map.of(DayOfWeek.FRIDAY.name(), "Sleep"));
        Set<String> dogHabits = new HashSet<>(Arrays.asList("Like biting", "Like bark"));
        Dog dog = new Dog("Мушу", 5, 60, dogHabits);

        Family family1 = new Family(mother1, father1);
        family1.addChild(childAlice);
        family1.addChild(childAnna);
        family1.setPet(dog);


        // ========== FAMILY 2 ================
        Map<String, String> father2Tasks = Map.of(DayOfWeek.FRIDAY.name(), "Go to the theatre with wife", "Saturday", "Go to gym");
        Human father2 = new Man("Stas", "Novikov", 1981, (byte) 80, father2Tasks);
        Human mother2 = new Woman("Darya", "Novikova", 1986);
        Human childMary = new Woman("Mary", "Novikova", 2015, (byte) 95, Map.of(DayOfWeek.MONDAY.name(), "Go to school"));
        Set<String> catHabits = new HashSet<>(Arrays.asList("Like meawwwiting", "Like sleeping"));
        Pet cat = new DomesticCat("Tomas", 5, 60, catHabits);

        Family family2 = new Family(mother2, father2);
        family2.addChild(childMary);
        family2.setPet(cat);

    List<Family> families = new ArrayList<>();
    families.add(family1);
    families.add(family2);

    }
}
