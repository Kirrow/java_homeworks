package homeworks_module_01.Homework_07_happy_family_4;

public enum ScheduleTypes {
    HOMEWORK("Do homework"),
    GYM("Go to gym"),
    WALK("Go to walk"),
    CINEMA("Go to cinema");

    String sceduleType;

    ScheduleTypes(String sceduleType) {
        this.sceduleType = sceduleType;
    }

}
