package homeworks_module_01.Homework_07_happy_family_4;

public enum BoysNames {
    LUCK("Luck"),
    JACK("Jack"),
    JOHN("John"),
    ROBERT("Robert"),
    JIM("Jim"),
    BOB("Bob");

    public String name;

    BoysNames(String name){
        this.name = name;
    }
}
