package homeworks_module_01.Homework_07_happy_family_4;

import java.util.*;

public class Family {
private Human mother;
private Human father;
private List<Human> children = new ArrayList<>();
private Set<Pet> pets = new HashSet<>();

//    static {
//        System.out.println("Завантажується новий клас Family");
//    }
//
//    {
//        System.out.println("Створений новий екземпляр об\'єкту Family");
//    }
    public Family(Human mother, Human father) {
        if (mother != null && father != null) {
            this.mother = mother;
            this.father = father;
            this.mother.setFamily(this);
            this.father.setFamily(this);
        } else {
            System.out.println("Error: Both mother and father are required to create a family.");
        }
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPet(Pet pet) {
        pets.add(pet);
    }

    public void addChild(Human child) {
        child.setFamily(this);
        children.add(child);
    }

    public boolean deleteChild(Human child) {
        if(children.contains(child)){
            children.remove(child);
            child.setFamily(null);
            return true;
        } else {
            return false;
        }
    }

    public int countFamily() {
        return 2 + children.size();
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pet=" + pets +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother.getName(), family.mother.getName())
                && Objects.equals(mother.getSurname(), family.mother.getSurname())
                && Objects.equals(father.getName(), family.father.getName())
                && Objects.equals(father.getSurname(), family.father.getSurname())
                && children.equals(family.children)
                && Objects.equals(pets, family.pets)
                && Objects.equals(pets.stream().map(x -> x.getNickname().toLowerCase()), family.pets.stream().map(x -> x.getNickname().toLowerCase()));
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pets);
        result = 31 * result + children.hashCode();
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Інформація про об'єкт, що видаляється: \n" + this);
    }
}
