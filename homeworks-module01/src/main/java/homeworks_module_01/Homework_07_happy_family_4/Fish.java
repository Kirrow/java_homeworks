package homeworks_module_01.Homework_07_happy_family_4;

import java.util.Set;

public class Fish extends Pet {
    public Species species = Species.FISH;

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);

    };
    public void respond(){
        System.out.printf("Рибка не вміє говорити! \n");
    };



}
