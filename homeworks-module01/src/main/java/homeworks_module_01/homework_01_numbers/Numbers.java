package homeworks_module_01.homework_01_numbers;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Collections;

public class Numbers {
    static String userString;
    static int num;

    static String[][] events = {
            {"1492", "Christopher Columbus reaches the Americas"},
            {"1789", "French Revolution begins"},
            {"1945", "End of World War II"},
            {"1969", "Apollo 11 moon landing"},
            {"1989", "Fall of the Berlin Wall"},
            {"2001", "September 11 attacks"},
            {"2011", "Arab Spring protests"},
            {"2020", "COVID-19 pandemic begins"}
    };

    public static void main(String[] args) {

        int a = (int) (Math.random() * events.length);
        String yearStr = events[a][0];
        int yearNum = Integer.parseInt(yearStr.trim());

        Scanner in = new Scanner(System.in);

        System.out.print("Enter your name: ");
        String name = in.nextLine();

        printMessage("Let the game begin! \n", false);
        printMessage(events[a][1], true);
        Integer[] arr = {};

        do{
            printEnterNumberMessage();

            if(num < yearNum) {
                printMessage("Your number is too small. Please, try again.", true);
                arr = extendArray(arr, num);
            } else if (num > yearNum){
                printMessage("Your number is too big. Please, try again.", true);
                arr = extendArray(arr, num);
            } else {
                System.out.printf("Congratulations, %s! \n" , name);
                arr = extendArray(arr, num);
                break;
            }
        } while (true);

        Arrays.sort(arr, Collections.reverseOrder());
        System.out.println("Your numbers: " + Arrays.toString(arr));

    }

    public static void printMessage(String message, boolean isWrapToNextLine){
        System.out.print(message + (isWrapToNextLine ? "\n" : ""));
    };

    public static void printEnterNumberMessage(){
        Scanner in = new Scanner(System.in);
        printMessage("Input a number: ", false);
        userString = in.nextLine();

        try{
            num = Integer.parseInt(userString.trim());
        } catch (NumberFormatException nfe){
            printEnterNumberMessage();
        }
    };

    public static Integer[] extendArray (Integer[] array, int value){
        Integer[] copiedArray = new Integer[array.length + 1];
        System.arraycopy(array, 0, copiedArray, 0, array.length);
        copiedArray[array.length] = value;

        return copiedArray;
    }
}
