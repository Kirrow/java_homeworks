package homeworks_module_01.Homework_04_happy_family;

import homeworks_module_01.Homework_04_happy_family.Human;
import homeworks_module_01.Homework_04_happy_family.Pet;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[][] father1Tasks = {{"Monday", "Do homework"}, {"Tuesday", "Go to pool"}};
        Human father1 = new Human("Petro", "Ivanov", 1980, (byte) 80, father1Tasks);
        Human mother1 = new Human("Maria", "Ivanova", 1985);
        Human childAlice = new Human("Alice", "Ivanova", 2010, (byte) 90, new String[][]{{"Monday", "Work"}});
        Human childAnna = new Human("Anna", "Ivanova", 2014, (byte) 85, new String[][]{{"Monday", "Sleep"}});
        String[] dogHabits = {"Like biting", "Like bark"};
        Pet dog = new Pet("dog", "Sharik", 5, 60, dogHabits);

        Family family1 = new Family(mother1, father1);
        family1.addChild(childAlice);
        family1.addChild(childAnna);
        family1.setPet(dog);

        // Call methods and print information
        System.out.println("Family Information:");
        System.out.println(family1);

        System.out.println("\nChild Information:");
        System.out.println(childAlice);
        childAlice.greetPet();
        childAlice.describePet();
        childAlice.feedPet(false);
        System.out.println(childAlice);
        Arrays.deepToString(childAlice.getSchedule());
        System.out.println(family1.countFamily());

        // Call methods of pet
        dog.getNickname();
        dog.getTrickLevel();
        dog.getAge();
        dog.eat();
        dog.foul();
        dog.getHabits();
        dog.respond();

        //=========Delete child ============
        System.out.println(Arrays.deepToString(family1.getChildren()));
        System.out.println("family before deleting\n" + Arrays.deepToString(family1.getChildren()));
        family1.deleteChild(childAnna);
        System.out.println("family after deleting\n" + Arrays.deepToString(family1.getChildren()));


        // ========== FAMILY 2 ================
        String[][] father2Tasks = {{"Friday", "Go to the theatre with wife"}, {"Saturday", "Go to gym"}};
        Human father2 = new Human("Stas", "Novikov", 1981, (byte) 80, father1Tasks);
        Human mother2 = new Human("Darya", "Novikova", 1986);
        Human childMary = new Human("Mary", "Novikova", 2015, (byte) 95, new String[][]{{"Monday", "Go to school"}});
        String[] catHabits = {"Like meawwwiting", "Like sleeping"};
        Pet cat = new Pet("cat", "Tomas", 5, 60, catHabits);

        Family family2 = new Family(mother2, father2);
        family2.addChild(childMary);
        family2.setPet(cat);
    }
}
