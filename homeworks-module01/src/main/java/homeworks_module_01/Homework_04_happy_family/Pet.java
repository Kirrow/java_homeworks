package homeworks_module_01.Homework_04_happy_family;

import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("Завантажується новий клас Pet");
    }

    {
        System.out.println("Створений новий екземпляр об\'єкту Pet");
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    };

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    };

    public Pet() {
    };

    public static void main(String[] args) {
        String[] dogHabbits = {"like biting"};
        Pet dog = new Pet("dog", "Sharik", 3, 49, dogHabbits);
        System.out.println(dog);
    };

    public String getSpecies() {
        return species;
    };

    public int getAge() {
        return age;
    };

    public int getTrickLevel() {
        return trickLevel;
    };

    public String getNickname() {
        return nickname;
    };

    public String[] getHabits() {
        return habits;
    }

    public void eat(){
        System.out.println("Я їм!!!");
    };

    public void respond(){
        System.out.printf("Привіт, хазяїн. Я - %s. Я скучив! \n", nickname);
    };

    public void foul(){
        System.out.println("Потрібно добре замести сліди...");
    };

    @Override
    public String toString() {
//        dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}
        return species + "{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.deepToString(habits) +
                '}';
    }
}
