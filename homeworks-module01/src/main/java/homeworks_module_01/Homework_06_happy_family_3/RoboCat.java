package homeworks_module_01.Homework_06_happy_family_3;

public class RoboCat extends Pet {
    public Species species = Species.ROBOCAT;

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);

    };
    public void respond(){
        System.out.printf("Привіт, мастер. Я - %s. Дай бензину! \n", super.getNickname());
    };

}
