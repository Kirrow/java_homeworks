package homeworks_module_01.Homework_06_happy_family_3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Family {
private Human mother;
private Human father;
private List<Human> children = new ArrayList<>();
private Pet pet;

//    static {
//        System.out.println("Завантажується новий клас Family");
//    }
//
//    {
//        System.out.println("Створений новий екземпляр об\'єкту Family");
//    }
    public Family(Human mother, Human father) {
        if (mother != null && father != null) {
            this.mother = mother;
            this.father = father;
            this.mother.setFamily(this);
            this.father.setFamily(this);
        } else {
            System.out.println("Error: Both mother and father are required to create a family.");
        }
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

//    public void setChildren(Human[] children) {
//        this.children = children;
//    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void addChild(Human child) {
        child.setFamily(this);
        children.add(child);
    }

    public boolean deleteChild(Human child) {
        if(children.contains(child)){
            children.remove(child);
            child.setFamily(null);
            return true;
        } else {
            return false;
        }
//        int indexToRemove = -1;
//        for (int i = 0; i < children.length; i++) {
//            if (children[i].equals(child)) {
//                indexToRemove = i;
//                break;
//            }
//        }
//
//
//        if (indexToRemove != -1) {
//            Human[] copyChildrenArray = new Human[children.length - 1];
//            System.arraycopy(children, 0, copyChildrenArray, 0, indexToRemove);
//            System.arraycopy(children, indexToRemove + 1, copyChildrenArray, indexToRemove, children.length - indexToRemove - 1);
//            setChildren(copyChildrenArray);
//            child.setFamily(null);
//            return true;
//        } else {
//            return false;
//        }
    }

    public int countFamily() {
        return 2 + children.size();
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pet=" + pet +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother.getName(), family.mother.getName())
                && Objects.equals(mother.getSurname(), family.mother.getSurname())
                && Objects.equals(father.getName(), family.father.getName())
                && Objects.equals(father.getSurname(), family.father.getSurname())
                && children.equals(family.children)
                && Objects.equals(pet.getSpecies(), family.pet.getSpecies())
                && Objects.equals(pet.getNickname(), family.pet.getNickname());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + children.hashCode();
        return result;
    }




    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Інформація про об'єкт, що видаляється: \n" + this);
    }
}
