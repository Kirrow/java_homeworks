package homeworks_module_01.Homework_06_happy_family_3;

public class Dog extends Pet implements Foul {
    public Species species = Species.DOG;

    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(Species.DOG);

    };
    public void respond(){
        System.out.printf("Привіт, хазяїн. Я - %s. Я скучив! \n", super.getNickname());
    };

}
