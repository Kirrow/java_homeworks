package homeworks_module_01.Homework_06_happy_family_3;

public final class Woman extends Human {

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, byte iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPet() {
        System.out.printf("Привіт, мілашка %s", super.family.getPet().getNickname());
    }

    public void makeup(){
        System.out.println("Підфарбувалася!");
    }

}
