package homeworks_module_01.Homework_06_happy_family_3;

public enum BoysNames {
    LUCK("Luck"),
    JACK("Jack"),
    JOHN("John"),
    ROBERT("Robert"),
    JIM("Jim"),
    BOB("Bob");

    public String name;

    BoysNames(String name){
        this.name = name;
    }
}
