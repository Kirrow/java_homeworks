package homeworks_module_01.Homework_06_happy_family_3;

public final class Man extends Human {

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, byte iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPet() {
        System.out.printf("Привіт, %s", super.family.getPet().getNickname());
    }

    public void repairCar(){
        System.out.println("Відремонтував авто!");
    }
}
