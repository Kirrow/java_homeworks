package homeworks_module_01.Homework_06_happy_family_3;

import java.util.Arrays;

public abstract class Pet {
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    //  static {
    //      System.out.println("Завантажується новий клас Pet");
    //  }
    //
    //  {
    //      System.out.println("Створений новий екземпляр об\'єкту Pet");
    //  }

    public Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    };

    public Pet(String nickname) {
        this.nickname = nickname;
    };

    public Species getSpecies() {
        return species;
    };

    public int getAge() {
        return age;
    };

    public int getTrickLevel() {
        return trickLevel;
    };

    public String getNickname() {
        return nickname;
    };

    public String[] getHabits() {
        return habits;
    }

    public void eat(){
        System.out.println("Я їм!!!");
    };

    public abstract void respond();

    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override
    public String toString() {
//        dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}
        return species + "{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.deepToString(habits) +
                '}';
    }

//    @Override
//    protected void finalize() throws Throwable {
//        super.finalize();
//        System.out.println("Інформація про об'єкт, що видаляється: \n" + this);
//    }
}
