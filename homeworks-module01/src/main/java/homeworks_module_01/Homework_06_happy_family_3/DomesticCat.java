package homeworks_module_01.Homework_06_happy_family_3;

public class DomesticCat extends Pet implements Foul {
    public Species species = Species.DOMESTICCAT;

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);

    };
    public void respond(){
        System.out.printf("Привіт!. Я - %s. Що в нас на вечерю? \n", super.getNickname());
    };

}