package homeworks_module_01.Homework_06_happy_family_3;

public enum ScheduleTypes {
    HOMEWORK("Do homework"),
    GYM("Go to gym"),
    WALK("Go to walk"),
    CINEMA("Go to cinema");

    String sceduleType;

    ScheduleTypes(String sceduleType) {
        this.sceduleType = sceduleType;
    }

}
