package homeworks_module_01.Homework_06_happy_family_3;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[][] father1Tasks = {{DayOfWeek.MONDAY.name(), "Do homework"}, {DayOfWeek.THURSDAY.name(), "Go to pool"}};
        Man father1 = new Man("Ivan", "Ivanov", 1980, (byte) 80, father1Tasks);
        Woman mother1 = new Woman("Masha", "Ivanova", 1985);
        Woman childAlice = new Woman("Alice", "Ivanova", 2010, (byte) 90, new String[][]{{DayOfWeek.SATURDAY.name(), "Work"}});
        Woman childAnna = new Woman("Anna", "Ivanova", 2014, (byte) 85, new String[][]{{DayOfWeek.FRIDAY.name(), "Sleep"}});
        String[] dogHabits = {"Like biting", "Like bark"};
        Dog dog = new Dog("Мушу", 5, 60, dogHabits);
        dog.respond();

        Family family1 = new Family(mother1, father1);
        family1.addChild(childAlice);
        family1.addChild(childAnna);
        family1.setPet(dog);

        System.out.println("=================//==================");
        father1.repairCar();
        mother1.makeup();
        System.out.println(father1.bornChild());

        // Call methods and print information
        System.out.println("=================//==================");
        System.out.println("Family Information:");
        System.out.println(family1);

        System.out.println("\nChild Information:");
        System.out.println(childAlice);
        childAlice.greetPet();
        childAlice.describePet();
        childAlice.feedPet(false);
        System.out.println(childAlice);
        Arrays.deepToString(childAlice.getSchedule());
        System.out.println(family1.countFamily());

        // Call methods of pet
        System.out.println("=================//==================");
        dog.getNickname();
        dog.getTrickLevel();
        dog.getAge();
        dog.eat();
        dog.foul();
        dog.getHabits();
        dog.respond();

        //=========Delete child ============
        System.out.println(family1.getChildren());
        System.out.println("family before deleting\n" + family1.getChildren());
        family1.deleteChild(childAnna);
        System.out.println("family after deleting\n" + family1.getChildren());


        // ========== FAMILY 2 ================
        String[][] father2Tasks = {{DayOfWeek.FRIDAY.name(), "Go to the theatre with wife"}, {"Saturday", "Go to gym"}};
        Human father2 = new Man("Stas", "Novikov", 1981, (byte) 80, father1Tasks);
        Human mother2 = new Woman("Darya", "Novikova", 1986);
        Human childMary = new Woman("Mary", "Novikova", 2015, (byte) 95, new String[][]{{DayOfWeek.MONDAY.name(), "Go to school"}});
        String[] catHabits = {"Like meawwwiting", "Like sleeping"};
        Pet cat = new DomesticCat("Tomas", 5, 60, catHabits);

        Family family2 = new Family(mother2, father2);
        family2.addChild(childMary);
        family2.setPet(cat);


        // Test methods


        // Test finalize method
        Human motherFinalizeTest = new Woman("Darya", "Novikova", 1986);
        Human fatherFinalizeTest = new Man("Stas", "Novikov", 1981, (byte) 80, father1Tasks);

//        for (int i=0; i < 1000000; i++){
//
//            Family family3 = new Family(motherFinalizeTest, fatherFinalizeTest);
//        }

    }
}
