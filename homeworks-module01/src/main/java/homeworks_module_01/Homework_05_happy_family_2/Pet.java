package homeworks_module_01.Homework_05_happy_family_2;

import java.util.Arrays;

public class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("Завантажується новий клас Pet");
    }

    {
        System.out.println("Створений новий екземпляр об\'єкту Pet");
    }

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    };

    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    };

    public Pet() {
    };

    public Species getSpecies() {
        return species;
    };

    public int getAge() {
        return age;
    };

    public int getTrickLevel() {
        return trickLevel;
    };

    public String getNickname() {
        return nickname;
    };

    public String[] getHabits() {
        return habits;
    }

    public void eat(){
        System.out.println("Я їм!!!");
    };

    public void respond(){
        System.out.printf("Привіт, хазяїн. Я - %s. Я скучив! \n", nickname);
    };

    public void foul(){
        System.out.println("Потрібно добре замести сліди...");
    };

    @Override
    public String toString() {
//        dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}
        return species + "{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.deepToString(habits) +
                '}';
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Інформація про об'єкт, що видаляється: \n" + this);
    }
}
