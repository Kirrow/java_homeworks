package homeworks_module_01.Homework_05_happy_family_2;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[][] father1Tasks = {{DayOfWeek.MONDAY.name(), "Do homework"}, {DayOfWeek.THURSDAY.name(), "Go to pool"}};
        Human father1 = new Human("Ivan", "Ivanov", 1980, (byte) 80, father1Tasks);
        Human mother1 = new Human("Masha", "Ivanova", 1985);
        Human childAlice = new Human("Alice", "Ivanova", 2010, (byte) 90, new String[][]{{DayOfWeek.SATURDAY.name(), "Work"}});
        Human childAnna = new Human("Anna", "Ivanova", 2014, (byte) 85, new String[][]{{DayOfWeek.FRIDAY.name(), "Sleep"}});
        String[] dogHabits = {"Like biting", "Like bark"};
        Pet dog = new Pet(Species.DOG, "Мушу", 5, 60, dogHabits);

        Family family1 = new Family(mother1, father1);
        family1.addChild(childAlice);
        family1.addChild(childAnna);
        family1.setPet(dog);

        // Call methods and print information
        System.out.println("Family Information:");
        System.out.println(family1);

        System.out.println("\nChild Information:");
        System.out.println(childAlice);
        childAlice.greetPet();
        childAlice.describePet();
        childAlice.feedPet(false);
        System.out.println(childAlice);
        Arrays.deepToString(childAlice.getSchedule());
        System.out.println(family1.countFamily());

        // Call methods of pet
        dog.getNickname();
        dog.getTrickLevel();
        dog.getAge();
        dog.eat();
        dog.foul();
        dog.getHabits();
        dog.respond();

        //=========Delete child ============
        System.out.println(Arrays.deepToString(family1.getChildren()));
        System.out.println("family before deleting\n" + Arrays.deepToString(family1.getChildren()));
        family1.deleteChild(childAnna);
        System.out.println("family after deleting\n" + Arrays.deepToString(family1.getChildren()));


        // ========== FAMILY 2 ================
        String[][] father2Tasks = {{DayOfWeek.FRIDAY.name(), "Go to the theatre with wife"}, {"Saturday", "Go to gym"}};
        Human father2 = new Human("Stas", "Novikov", 1981, (byte) 80, father1Tasks);
        Human mother2 = new Human("Darya", "Novikova", 1986);
        Human childMary = new Human("Mary", "Novikova", 2015, (byte) 95, new String[][]{{DayOfWeek.MONDAY.name(), "Go to school"}});
        String[] catHabits = {"Like meawwwiting", "Like sleeping"};
        Pet cat = new Pet(Species.CAT, "Tomas", 5, 60, catHabits);

        Family family2 = new Family(mother2, father2);
        family2.addChild(childMary);
        family2.setPet(cat);


        // Test finalize method
        Human motherFinalizeTest = new Human("Darya", "Novikova", 1986);
        Human fatherFinalizeTest = new Human("Stas", "Novikov", 1981, (byte) 80, father1Tasks);

//        for (int i=0; i < 1000000; i++){
//
//            Family family3 = new Family(motherFinalizeTest, fatherFinalizeTest);
//        }

    }
}
