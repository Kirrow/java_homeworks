package homeworks_module_01.Homework_05_happy_family_2;

import java.util.Arrays;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private int year;
    private byte iq;
    private String[][] schedule;
    private Family family;
    static {
        System.out.println("Завантажується новий клас Human");
    }

    {
        System.out.println("Створений новий екземпляр об\'єкту Human");
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    };

     public Human(String name, String surname, int year, byte iq, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    };

    public Human() {
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public byte getIq() {
        return (byte) iq;
    }

    public void setIq(byte iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Human getMother() {
        return family.getMother();
    }

    public void setMother(Human mother) {
        family.setMother(mother);
    }

    public Human getFather() {
        return family.getFather();
    }

    public void setFather(Human father) {
        family.setFather(father);
    }
    public void greetPet(){
        System.out.printf("Привіт, %s", family.getPet().getNickname());
    };


    public void describePet(){
        String trickLevel = family.getPet().getTrickLevel() > 50 ? "дуже хитрий" : "майже не хитрий";
        System.out.printf("\nУ мене є %s, йому %d років, він %s", family.getPet().getSpecies(), family.getPet().getAge(), trickLevel);
    };

    public void feedPet(boolean isItTimeToFeed){
        if(isItTimeToFeed) {
            System.out.printf("\n%s нагодував свого домашього улюбленця %s \n", this.getName(), family.getPet().getNickname());
        };
        int randomNum = (int) (Math.random() * 100);
        System.out.printf("Хм... годувати %s чи %s не голодний... \n", family.getPet().getNickname(), family.getPet().getNickname());
        if(family.getPet().getTrickLevel() > randomNum){
            System.out.println("Все ж таки головний, покормив...");
        } else {
            System.out.println("%s все ж таки не головний, прикидається...");
        }
    };

    public void setFamily(Family family) {
        this.family = family;
    }
    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq, family);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Інформація про об'єкт, що видаляється: \n" + this);
    }
}
