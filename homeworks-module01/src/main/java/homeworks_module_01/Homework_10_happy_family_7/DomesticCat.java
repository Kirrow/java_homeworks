package homeworks_module_01.Homework_10_happy_family_7;

import java.util.Set;

public class DomesticCat extends Pet implements Foul {
    public Species species;

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(Species.DOMESTICCAT);
    };
    public void respond(){
        System.out.printf("Привіт!. Я - %s. Що в нас на вечерю? \n", super.getNickname());
    };

}