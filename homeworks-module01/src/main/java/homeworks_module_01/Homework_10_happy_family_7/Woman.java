package homeworks_module_01.Homework_10_happy_family_7;

import java.util.Map;

public class Woman extends Human {

    public Woman(String name, String surname, long birthDate) {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, long birthDate, byte iq, Map<String, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    @Override
    public void greetPet(String typeOfPet) {
        Pet chosenPet = super.chosePetFromList(typeOfPet);
        System.out.printf("Привіт, мілашка %s \n", chosenPet.getNickname());
    }

    public void makeup(){
        System.out.println("Підфарбувалася!");
    }

}
