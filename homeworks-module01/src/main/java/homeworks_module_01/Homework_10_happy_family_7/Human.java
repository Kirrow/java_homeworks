package homeworks_module_01.Homework_10_happy_family_7;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Human implements HumanCreator {
    private String name;
    private String surname;
    private long birthDate;
//    long time = Instant.now().getEpochSecond();
//    Instant instant = Instant.ofEpochSecond(time);
    private byte iq;
    private Map<String, String> schedule;
    Family family;


    public Human(String name, String surname, long birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    };

     public Human(String name, String surname, long birthDate, byte iq, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = schedule;
    };

    public Human(String name, String surname, String birthDateString, byte iq) {
        this.name = name;
        this.surname = surname;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = LocalDate.parse(birthDateString, formatter);
        Instant instant = localDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
        this.birthDate = instant.toEpochMilli();
        this.iq = iq;
    };

    public Human(){
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getYear() {
        return birthDate;
    }

    public void setYear(int year) {
        this.birthDate = year;
    }

    public byte getIq() {
        return (byte) iq;
    }

    public void setIq(byte iq) {
        this.iq = iq;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public Human getMother() {
        return family.getMother();
    }

    public void setMother(Human mother) {
        family.setMother(mother);
    }

    public Human getFather() {
        return family.getFather();
    }

    public void setFather(Human father) {
        family.setFather(father);
    }
    public void greetPet(String typeOfPet){
        String nickNameOfPet = "";
        for (Pet pet : family.getPets()) {
            if(pet.getSpecies().pet.toLowerCase() == typeOfPet.toLowerCase()){
                nickNameOfPet = pet.getNickname();
            }
        }
        System.out.printf("Привіт, %s", nickNameOfPet);
    };


    public void describePet(String typeOfPet){
        Pet petChosen = chosePetFromList(typeOfPet);

        String trickLevelString = petChosen.getTrickLevel() > 50 ? "дуже хитрий" : "майже не хитрий";
        System.out.printf("\nУ мене є %s, йому %d років, він %s", petChosen.getSpecies().pet, petChosen.getAge(), trickLevelString);
    };

    public void feedPet(boolean isItTimeToFeed, String petType){
        Pet petChosen = chosePetFromList(petType);
        if(isItTimeToFeed) {
            System.out.printf("\n%s нагодував свого домашього улюбленця %s \n", this.getName(), petChosen.getNickname());
        };
        int randomNum = (int) (Math.random() * 100);
        System.out.printf("Хм... годувати %s чи %s не голодний... \n", petChosen.getNickname(), petChosen.getNickname());
        if(petChosen.getTrickLevel() > randomNum){
            System.out.printf("%s все ж таки голодний, покормив... \n", petChosen.getNickname());
        } else {
            System.out.printf("%s не голодний, прикидається... \n", petChosen.getNickname());
        }
    };

    public String describeAge(){
        LocalDate dateOfBirthFormatted = LocalDate.ofEpochDay(birthDate / (60 * 60 * 24 * 1000));
        LocalDate now = LocalDate.now();

        Period ageOfHuman = Period.between(now, dateOfBirthFormatted);
        int years = ageOfHuman.getYears();
        int months = ageOfHuman.getMonths();
        int days = ageOfHuman.getDays();

        return String.format("Person's age: %d years, %d months and %d days.", years, months, days);
    };

    public Pet chosePetFromList(String petType){
        for (Pet pet : family.getPets()) {
            if(pet.getSpecies().pet.toLowerCase() == petType.toLowerCase()){
                return pet;
            }
        }
        return null;
    };

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public Human bornChild() {
        boolean isNewChildMan = Math.random() < 0.5;
        byte iqChild = (byte) ((byte) (family.getFather().getIq() + family.getMother().getIq()) / 2);

        String childRandomName = getChildRandomName(isNewChildMan);
        String childSurname = family.getFather().surname;
        Human child = isNewChildMan ? new Man(childRandomName, childSurname, 2024, iqChild, new HashMap<>()) : new Woman(childRandomName, childSurname, 2024, iqChild, new HashMap<>());
        family.addChild(child);
        return child;
    };

    String getChildRandomName(boolean isNewChildMan) {
        int countOfAvailableNames = isNewChildMan ? BoysNames.values().length : GirlsNames.values().length;
        int randomNameIndex = (int) (Math.random() * countOfAvailableNames);

        String childName = "";
        if(isNewChildMan){
            for (int i = 0; i < BoysNames.values().length; i++) {
                if (i == randomNameIndex) {
                    childName = BoysNames.values()[i].name;
                }
            }
        } else {
            for (int i = 0; i < GirlsNames.values().length; i++) {
                if (i == randomNameIndex) {
                    childName = GirlsNames.values()[i].name;
                }
            }
        }


        return childName;
    }

    @Override
    public String toString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = Instant.ofEpochMilli(birthDate).atZone(ZoneId.systemDefault()).toLocalDate();
        String formattedBirthDate = localDate.format(formatter);

        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + formattedBirthDate +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq, family);
    }


    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Інформація про об'єкт, що видаляється: \n" + this);
    }
}
