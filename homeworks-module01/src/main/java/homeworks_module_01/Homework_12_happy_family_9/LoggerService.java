package homeworks_module_01.Homework_12_happy_family_9;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.logging.*;

public class LoggerService {
    private static Logger logger;

    static {
        try {
            logger = Logger.getLogger(LoggerService.class.getName());
            FileHandler fileHandler = new FileHandler("application.log");
            fileHandler.setFormatter(new SimpleFormatter());
            logger.addHandler(fileHandler);
            logger.setUseParentHandlers(false);
        } catch (IOException e){
            System.out.println("ERROR " + e);
        }
    }

    static void info(String message){
        log("INFO", message);
    }

    static void error(String message){
        log("ERROR", message);
    }

    private static void log(String level, String message) {
        String timeNow = LocalDateTime.now().toString();

        String logMessage = String.format("%s [%s] %s", timeNow, level, message);

        logger.info(logMessage);

        FileHandler fileHandler = (FileHandler) logger.getHandlers()[0];
        fileHandler.setFormatter(new SimpleFormatter());

        LogRecord logRecord = new LogRecord(Level.INFO, logMessage);
        fileHandler.publish(logRecord);
    }
}
