package homeworks_module_01.Homework_12_happy_family_9;

import java.util.ArrayList;
import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    Boolean deleteFamily(int index);
    Boolean deleteFamily(Family family);
    boolean saveFamilyToBase(Family family);
    void loadData (ArrayList<Family> families);
    void loadDataFromFile ();
    void saveDataToAFile();

}
