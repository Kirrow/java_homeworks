package homeworks_module_01.Homework_12_happy_family_9;

public class FamilyOverflowException extends RuntimeException {
        public FamilyOverflowException() {
            super("Family size exceeds the limit.");
        }

        public FamilyOverflowException(String message) {
            super(message);
        }

}
