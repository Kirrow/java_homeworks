package homeworks_module_01.Homework_12_happy_family_9;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {

    FamilyDao familyDao;
    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies(){
        return familyDao.getAllFamilies();
    }

    public void displayFamily(int index) {
        Family family = familyDao.getFamilyByIndex(index);
        if (family != null) {
            System.out.println(family.prettyFormat());
        } else {
            System.out.println("Family not found.");
        }
    }

    public void displayAllFamilies(){
        List<Family> families = getAllFamilies();
        int count = 1;
        if(families.size() == 0){
            System.out.println("No families found");
        }

        for (Family family : families) {
            System.out.printf("Family # %d: %s\n", count++, family.prettyFormat());
        }
    }

    public String generateRandomBirthdayString(boolean isParent){
        String randomYear = isParent
                ? String.valueOf(1985 + (int) (Math.random()*25))
                : String.valueOf(2010 + (int) (Math.random()*13));
        String randomDay = String.valueOf((int) (10 + Math.random()*18));
        String randomMonth = String.valueOf((int) (1 + Math.random()*8));

        return randomDay + "/0" + randomMonth + "/" + randomYear;
    }
    public void createMockFamilies() {
        int countOfFamilies = (int) (3 + Math.random()*2);
        System.out.println("Test1: countOfFamilies - " + countOfFamilies);
        for (int i = 0; i < countOfFamilies; i++) {

            Human mother = new Human("Mother" + (i + 1), "Surname" + (i + 1), generateRandomBirthdayString(true), (byte) 110);
            Human father = new Human("Father" + (i + 1), "Surname" + (i + 1), generateRandomBirthdayString(true), (byte) 120);

            Family family = createNewFamily(father, mother);
            int countOfChildren = (int) (Math.random() * 3);

            for (int c = 0; c < countOfChildren; c++) {
                Human child = (c % 2 == 0)
                        ? new Human("Boy" + (c + 1), father.getSurname(), generateRandomBirthdayString(false), (byte) 105)
                        : new Human("Girl" + (c + 1), mother.getSurname(), generateRandomBirthdayString(false), (byte) 105 );

                family.addChild(child);
            }

        }
    }

    public List<Family> getFamiliesBiggerThan(int familyBiggerThan){
        List<Family> allFamilies = getAllFamilies();
        List<Family> filteredFamilies = allFamilies.stream().filter(family -> family.countFamily() > familyBiggerThan).collect(Collectors.toList());
        return filteredFamilies;
    }

    public List<Family> getFamiliesLessThan(int familyLessThan){
        List<Family> allFamilies = getAllFamilies();
        List<Family> filteredFamilies = allFamilies.stream().filter(family -> family.countFamily() < familyLessThan).collect(Collectors.toList());
        return filteredFamilies;
    }

    public int countFamiliesWithMemberNumber(int countOfPeopleInFamily){
        List<Family> allFamilies = getAllFamilies();
        int filteredFamiliesByPeopleNumber = (int) allFamilies.stream().filter(family -> family.countFamily() == countOfPeopleInFamily).count();
        return filteredFamiliesByPeopleNumber;
    }

    public Family createNewFamily(Human father, Human mother){
        Family newFamily = new Family(mother, father);
        familyDao.saveFamilyToBase(newFamily);
        return newFamily;
    }

    public boolean deleteFamilyByIndex(int indexOfFamily){
        List<Family> families = getAllFamilies();
        if (indexOfFamily >= 0 && indexOfFamily < families.size()) {
            familyDao.deleteFamily(indexOfFamily);
            return true;
        }
        return false;
    }

    public Family bornChild(Family family, String maleName, String femaleName){
        Human child = new Human();
        boolean isNewChildMan = Math.random() < 0.5;

        child.setName(isNewChildMan ? maleName : femaleName);
        child.setSurname(family.getFather().getSurname() + (isNewChildMan ? "" : "a"));
        family.addChild(child);
        familyDao.saveFamilyToBase(family);
        return family;
    }

    public Family adoptChild(Family family, Human childToAdoption){
        family.addChild(childToAdoption);
        familyDao.saveFamilyToBase(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age){
        List<Family> families = getAllFamilies();
        for (Family family :
                families) {
            family.getChildren().removeIf(child -> (child.getYear() > age));
            familyDao.saveFamilyToBase(family);
        }
    }

    public int count(){
        return getAllFamilies().size();
    }

    Family getFamilyById(int familyIndex){
        return familyDao.getFamilyByIndex(familyIndex);
    }

    Set<Pet> getPets(int familyIndex){
        return getFamilyById(familyIndex).getPets();
    }

    public void addPet(int familyIndex, Pet newPet){
        Family currentFamily = getFamilyById(familyIndex);
        currentFamily.setPet(newPet);
        familyDao.saveFamilyToBase(currentFamily);
    }

    public void loadDataFromFile() {
        familyDao.loadDataFromFile();
    }

    public void saveDataToAFile(){
        familyDao.saveDataToAFile();
    }

}
