package homeworks_module_01.Homework_12_happy_family_9;

import java.util.Set;

public class Dog extends Pet implements Foul {
    public Species species = Species.DOG;

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(Species.DOG);

    };
    public void respond(){
        System.out.printf("Привіт, хазяїн. Я - %s. Я скучив! \n", super.getNickname());
    };

}
