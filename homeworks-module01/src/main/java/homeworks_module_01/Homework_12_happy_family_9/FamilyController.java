package homeworks_module_01.Homework_12_happy_family_9;

import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class FamilyController {
    public FamilyService familyService;
    public UserMenu userMenu;
    int MAX_FAMILY_SIZE = 5;
    Scanner scanner = new Scanner(System.in);

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
        this.userMenu = new UserMenu();
    }

    public List<Family> getAllFamilies(){
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies(){
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int familyBiggerThan){
        return familyService.getFamiliesBiggerThan(familyBiggerThan);
    }

    public List<Family> getFamiliesLessThan(int familyLessThan){
        return familyService.getFamiliesLessThan(familyLessThan);
    }

    public int countFamiliesWithMemberNumber(int countOfPeopleInFamily){
        return familyService.countFamiliesWithMemberNumber(countOfPeopleInFamily);
    }

    public void createNewFamily(Human father, Human mother){
        familyService.createNewFamily(father, mother);
    }

    public boolean deleteFamilyByIndex(int indexOfFamily){
        return familyService.deleteFamilyByIndex(indexOfFamily);
    }

    public Family bornChild(Family family, String maleName, String femaleName){
        if (family.countFamily() > MAX_FAMILY_SIZE) {
            throw new FamilyOverflowException();
        }
        return familyService.bornChild(family, maleName, femaleName);
    }

    public Family adoptChild(Family family, Human childToAdoption){
        if (family.countFamily() > MAX_FAMILY_SIZE) {
            throw new FamilyOverflowException();
        }
        return familyService.adoptChild(family, childToAdoption);
    }

    public void deleteAllChildrenOlderThen(int age){
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count(){
        return familyService.count();
    }

    Family getFamilyById(int familyIndex){
        return familyService.getFamilyById(familyIndex);
    }

    Set<Pet> getPets(int familyIndex){
        return familyService.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet newPet){
        familyService.addPet(familyIndex, newPet);
    }

    public void createMockFamilies(){
        familyService.createMockFamilies();
    }
    static boolean isNumber(String s ){
        return s.matches("\\d+\\.\\d+") || s.matches("\\d+");
    }
    public void displayMenu() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            userMenu.printMenu();
            System.out.println("Enter menu number: ");
            int menuNumber = getChoice();

            switch (menuNumber) {
                case 1:
                    createMockFamilies();
                    break;
                case 2:
                    saveDataToAFile();
                    break;
                case 3:
                    loadDataFromFile();
                    break;
                case 4:
                    displayAllFamilies();
                    break;
                case 5: {
                    System.out.println("Enter number of family members from:");
                    int biggerThan = scanner.nextInt();
                    List<Family> familiesBigger = getFamiliesBiggerThan(biggerThan);
                    if (familiesBigger.size() > 0) {
                        System.out.printf("Families with count of members bigger, than %d:\n", biggerThan);
                    } else {
                        System.out.printf("It is absent such families with amount of members bigger than %s\n", biggerThan);
                    }
                    ;
                    int index = 0;
                    for (Family family : familiesBigger) {
                        System.out.printf("Family #%d: %n %s\n", ++index, family.prettyFormat());
                    }
                    break;
                }
                case 6: {
                    System.out.println("Enter number of family members to:");
                    int lessThan = scanner.nextInt();
                    List<Family> familiesLess = getFamiliesLessThan(lessThan);
                    if (familiesLess.size() > 0) {
                        System.out.printf("Families with count of members less, than %d:\n", lessThan);
                    } else {
                        System.out.printf("It is absent such families with amount of members less than %d\n", lessThan);
                    }
                    ;
                    int index = 1;
                    for (Family family : familiesLess) {
                        System.out.printf("Family #%d: %n %s\n", index++, family.prettyFormat());
                    }
                    break;
                }
                case 7:
                    System.out.println("Enter amount of members in family:");
                    int membersAmount  = scanner.nextInt();
                    if(countFamiliesWithMemberNumber(membersAmount) > 0){
                        System.out.printf("There are %d families with %d members\n", countFamiliesWithMemberNumber(membersAmount), membersAmount);
                    } else {
                        System.out.printf("It is absent such families with %d amount of members\n", membersAmount);
                    }
                    break;
                case 8:
                    System.out.println("Creating family, please enter next information:");
                    System.out.println("Mother's name:");
                    String motherName = scanner.nextLine();
                    System.out.println("Mother's surname:");
                    String motherSurname = scanner.nextLine();
                    System.out.println("Mother's year of birth:");
                    int motherBirthYear = getYearChoise();
                    System.out.println("Mother's month of birth:");
                    int motherBirthMonth = getMonthChoice();
                    System.out.println("Mother's day of birth:");
                    int motherBirthDay = getDayChoice();
                    System.out.println("Mother's iq:");
                    byte motherIq = (byte) getIqChoise();
                    System.out.println("Father's name:");
                    String fatherName = scanner.next();
                    System.out.println("Father's surname:");
                    String fatherSurname = scanner.next();
                    System.out.println("Father's year of birth:");
                    int fatherBirthYear = getYearChoise();
                    System.out.println("Father's month of birth:");
                    int fatherBirthMonth = getMonthChoice();
                    System.out.println("Father's day of birth:");
                    int fatherBirthDay = getDayChoice();
                    System.out.println("Father's iq:");
                    byte fatherIq = (byte) getIqChoise();
                    Human mother = new Human(motherName, motherSurname, motherBirthYear, motherBirthMonth, motherBirthDay, motherIq);
                    Human father = new Human(fatherName, fatherSurname, fatherBirthYear, fatherBirthMonth, fatherBirthDay, fatherIq);
                    Family newFamily = familyService.createNewFamily(mother, father);
                    System.out.println(newFamily.prettyFormat());
                    break;
                case 9:
                    System.out.println("Enter index of family to delete:");
                    int deleteIndex = scanner.nextInt();
                    if (deleteFamilyByIndex(--deleteIndex)) {
                        System.out.println("Family was deleted.");
                    } else {
                        System.out.println("Wrong index of family.");
                    }
                    break;
                case 10:
                    boolean goToMainMenu = false;
                    System.out.println("Begin to edit family.");
                    while (!goToMainMenu) {
                        userMenu.printEditFamilySubMenu();
                        int userChoose = getChoice();
                        switch (userChoose) {
                            case 1:
                                System.out.println("Enter index of family to born child:");
                                int bornFamily = scanner.nextInt();
                                System.out.println("Enter name for boy:");
                                String boyName = scanner.nextLine();
                                System.out.println("Enter name for girl:");
                                String girlName = scanner.nextLine();
                                System.out.println(bornChild(getFamilyById(--bornFamily), boyName, girlName).prettyFormat());
                                break;
                            case 2:
                                System.out.println("Enter index of family to adopt child:");
                                int adoptFamily = scanner.nextInt();
                                System.out.println("Enter name of child:");
                                String adoptName = scanner.next();
                                System.out.println("Enter surname of child:");
                                String adoptSurname = scanner.next();
                                System.out.println("Enter year of birth:");
                                int adoptYear = getYearChoise();
                                System.out.println("Enter iq:");
                                int adoptIq = getIqChoise();
                                Human adoptedChild = new Human(adoptName, adoptSurname, adoptYear, (byte) adoptIq);
                                System.out.println(adoptChild(getFamilyById(--adoptFamily), adoptedChild).prettyFormat());
                                break;
                            case 3:
                                goToMainMenu = true;
                                break;
                        }
                    }
                    break;
                case 11:
                    System.out.println("Enter age of children for deleting.");
                    int ageToDelete = scanner.nextInt();
                    deleteAllChildrenOlderThen(ageToDelete);
                    break;
                case 0:
                    System.out.println("Thank you! Good luck!");
                    System.exit(0);
                    scanner.close();
                    break;
                default:
                    System.out.println("Wrong menu number. Please, try again.");
            }

        }
    }
    private int getChoice(){
        int choice;

        do {
            try {
                choice = Integer.parseInt(scanner.nextLine());
                return choice;
            } catch (NumberFormatException e){
                System.out.println("Error! Enter a number.");
            }
        } while(true);
    }

    private int getYearChoise(){
        int choice;
        do {
            try {
                choice = Integer.parseInt(scanner.nextLine());
                if(choice > 2024 || choice < 1900){
                    throw new NumberFormatException();
                }
                return choice;
            } catch (NumberFormatException e){
                System.out.println("Error! Enter iq number in range 1900-2024.");
            }
        } while(true);
    }
    private int getMonthChoice(){
        int choice;

        do {
            try {
                choice = Integer.parseInt(scanner.nextLine());
                if(choice > 12 || choice < 1){
                    throw new NumberFormatException();
                }
                return choice;
            } catch (NumberFormatException e){
                System.out.println("Error! Enter correct month number.");
            }
        } while(true);
    }

    private int getDayChoice(){
        int choice;
        do {
            try {
                choice = Integer.parseInt(scanner.nextLine());
                if(choice > 30 || choice < 1){
                    throw new NumberFormatException();
                }
                return choice;
            } catch (NumberFormatException e){
                System.out.println("Error! Enter correct day of month number.");
            }
        } while(true);
    }

    private byte getIqChoise(){
        int choice;
        do {
            try {
                choice = Integer.parseInt(scanner.nextLine());
                if(choice > 127 || choice < 1){
                    throw new NumberFormatException();
                }
                return (byte) choice;
            } catch (NumberFormatException e){
                System.out.println("Error! Enter iq number in range 0-127.");
            }
        } while(true);
    }

    public void loadDataFromFile() {
        familyService.loadDataFromFile();
    }

    public void saveDataToAFile(){
        System.out.println("Fc saveDataToAFile");
        familyService.saveDataToAFile();
    }

}
