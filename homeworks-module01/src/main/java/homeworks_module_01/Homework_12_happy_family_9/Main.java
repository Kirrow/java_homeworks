package homeworks_module_01.Homework_12_happy_family_9;

import java.io.File;

public class Main {
    private static final String FAMILY_DATA_FILE_NAME = "familiesData.bin";
    public static void main(String[] args) {

        File file = new File(FAMILY_DATA_FILE_NAME);
        FamilyDao familyDao = new CollectionFamilyDao(file);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.displayMenu();
        familyController.displayAllFamilies();
    }
}
