package homeworks_module_01.Homework_12_happy_family_9;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    ArrayList<Family> families;
    private final File file;

    public CollectionFamilyDao(File file) {
        this.families = new ArrayList<>();
        this.file = file;
    }

    @Override
    public List<Family> getAllFamilies() {
        LoggerService.info("Getting list of families.");
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        LoggerService.info("Receiving family by index.");
        return families.get(index);
    }

    @Override
    public Boolean deleteFamily(int index) {
        if (index >= 0 && index < families.size()) {
            families.remove(index);
            LoggerService.info("Deleting family by index.");
            return true;
        }
            return false;
    }

    @Override
    public Boolean deleteFamily(Family familyToDelete) {
        if (families.contains(familyToDelete)) {
            families.remove(familyToDelete);
            LoggerService.info("Deleting family.");
            return true;
        }
            return false;
    }

    @Override
    public void loadData(ArrayList<Family> families) {
        LoggerService.info("Loading families to local database");
        this.families = families;
    }

    public void loadDataFromFile() {
        if (this.file.exists()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(this.file))) {
                ArrayList families = (ArrayList<Family>) ois.readObject();
                loadData(families);
                LoggerService.info("Loading families to database from the file.");
            } catch (IOException | ClassNotFoundException e) {
                LoggerService.error("Error during loading families to database from the file.");
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean saveFamilyToBase(Family family) {
        int indexOfFamily = families.indexOf(family);
        if (indexOfFamily != -1) {
            families.set(indexOfFamily, family);
        } else {
            families.add(family);
        }
        LoggerService.info("Saving family to database.");
        return true;
    }

    public void saveDataToAFile() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(this.file))) {
            oos.writeObject(this.families);
            System.out.println("FDAO saveDataToAFile success");
            LoggerService.info("Saving families to the file from families dataBase.");
        } catch (IOException e) {
            LoggerService.error("Error during saving families to the file from families dataBase.");
            e.printStackTrace();
        }
    }
}
