package homeworks_module_01.Homework_08_happy_family_5;

public enum BoysNames {
    LUCK("Luck"),
    JACK("Jack"),
    JOHN("John"),
    ROBERT("Robert"),
    JIM("Jim"),
    BOB("Bob");

    public String name;

    BoysNames(String name){
        this.name = name;
    }
}
