package homeworks_module_01.Homework_08_happy_family_5;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        // ============= FAMILY 1 =====================
        Map<String, String> father1Tasks = Map.of(DayOfWeek.MONDAY.name(), "Do homework", DayOfWeek.THURSDAY.name(), "Go to pool");
        Human father1 = new Man("Ivan", "Ivanov", 1980, (byte) 80, father1Tasks);
        Human mother1 = new Woman("Masha", "Ivanova", 1985);
//        Woman childAlice = new Woman("Alice", "Ivanova", 2010, (byte) 90, Map.of(DayOfWeek.SATURDAY.name(), "Work"));
//        Woman childAnna = new Woman("Anna", "Ivanova", 2014, (byte) 85, Map.of(DayOfWeek.FRIDAY.name(), "Sleep"));
//        Set<String> dogHabits = new HashSet<>(Arrays.asList("Like biting", "Like bark"));
//        Dog dog = new Dog("Мушу", 5, 60, dogHabits);

//        Family family1 = new Family(mother1, father1);
//        family1.addChild(childAlice);
//        family1.addChild(childAnna);
//        family1.setPet(dog);


        // ========== FAMILY 2 ================
        Map<String, String> father2Tasks = Map.of(DayOfWeek.FRIDAY.name(), "Go to the theatre with wife", "Saturday", "Go to gym");
        Human father2 = new Man("Stas", "Novikov", 1981, (byte) 80, father2Tasks);
        Human mother2 = new Woman("Darya", "Novikova", 1986);
//        Human childMary = new Woman("Mary", "Novikova", 2015, (byte) 95, Map.of(DayOfWeek.MONDAY.name(), "Go to school"));
//        Set<String> catHabits = new HashSet<>(Arrays.asList("Like meawwwiting", "Like sleeping"));
//        Pet cat = new DomesticCat("Tomas", 5, 60, catHabits);

//        Family family2 = new Family(mother2, father2);
//        family2.addChild(childMary);
//        family2.setPet(cat);

        // ========== FAMILY 3 ================
        Map<String, String> father3Tasks = Map.of(DayOfWeek.FRIDAY.name(), "Go to the gym", "Saturday", "Watch TV");
        Human father3 = new Man("Petro", "Petrov", 1989, (byte) 90, father2Tasks);
        Human mother3 = new Woman("Nata", "Petrova", 1990);

        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.createNewFamily(father1, mother1);
        familyController.createNewFamily(father2, mother2);
        familyController.createNewFamily(father3, mother3);

        System.out.println("============familyController.getAllFamilies() ==============");
        System.out.println(familyController.getAllFamilies());

        System.out.println("============familyController.displayAllFamilies() ==============");
        familyController.displayAllFamilies();

        System.out.println("============family1.bornChild() ==============");
        Family family2 = familyController.getFamilyById(1);
        Family family3 = familyController.getFamilyById(2);
        familyController.bornChild(family2, "Vova", "Zina");
        familyController.bornChild(family3, "Ivan", "Gabriela");
        familyController.bornChild(family3, "Michael", "Tina");
        familyController.displayAllFamilies();

        System.out.println("============familyController.getFamiliesBiggerThan(2) ==============");
        System.out.println(familyController.getFamiliesBiggerThan(2));

        System.out.println("============familyController.getFamiliesLessThan(3) ==============");
        System.out.println(familyController.getFamiliesLessThan(3));

        System.out.println("============familyController.countFamiliesWithMemberNumber(2) ==============");
        System.out.println(familyController.countFamiliesWithMemberNumber(2));

        System.out.println("============familyController.deleteFamilyByIndex(1) ==============");
        System.out.println("Before:");
        familyController.displayAllFamilies();
        System.out.println("After deleting family with index 1:");
        familyController.deleteFamilyByIndex(1);
        familyController.displayAllFamilies();

        System.out.println("============familyController.adoptChild(family1, child) ==============");
        Family family1 = familyController.getFamilyById(0);
        Human childMary = new Woman("Mary", "Novikova", 15, (byte) 95, Map.of(DayOfWeek.MONDAY.name(), "Go to school"));
        System.out.println(familyController.adoptChild(family1, childMary));

        System.out.println("============familyController.deleteAllChildrenOlderThen(10) ==============");
        System.out.println("Before:");
        familyController.displayAllFamilies();
        System.out.println("After deleting children older 10 years");
        familyController.deleteAllChildrenOlderThen(10);
        familyController.displayAllFamilies();

        System.out.println("============familyController.count() ==============");
        System.out.println(familyController.count());

        System.out.println("============familyController.addPet() ==============");
        Set<String> dogHabits = new HashSet<>(Arrays.asList("Like biting", "Like bark"));
        Dog dog = new Dog("Мушу", 5, 60, dogHabits);
        Set<String> catHabits = new HashSet<>(Arrays.asList("Like meawwwiting", "Like sleeping"));
        Pet cat = new DomesticCat("Tomas", 5, 60, catHabits);
        familyController.addPet(0, dog);
        familyController.addPet(1, cat);
        familyController.displayAllFamilies();

        System.out.println("============familyController.getPets() ==============");
        System.out.println("Pets of family1:");
        System.out.println(familyController.getPets(0));
        System.out.println("Pets of family2:");
        System.out.println(familyController.getPets(1));
    }
}
