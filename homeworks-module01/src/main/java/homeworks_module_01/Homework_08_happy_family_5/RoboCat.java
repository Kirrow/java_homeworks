package homeworks_module_01.Homework_08_happy_family_5;

import java.util.Set;

public class RoboCat extends Pet {
    public Species species = Species.ROBOCAT;

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);

    };
    public void respond(){
        System.out.printf("Привіт, мастер. Я - %s. Дай бензину! \n", super.getNickname());
    };

}
