package homeworks_module_01.Homework_08_happy_family_5;

import java.util.Set;

public abstract class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;

    //  static {
    //      System.out.println("Завантажується новий клас Pet");
    //  }
    //
    //  {
    //      System.out.println("Створений новий екземпляр об\'єкту Pet");
    //  }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.species = Species.UNKNOWN;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    };

    public Pet(String nickname) {
        this.nickname = nickname;
    };

    public Species getSpecies() {
        return species;
    };

    public int getAge() {
        return age;
    };

    public int getTrickLevel() {
        return trickLevel;
    };

    public String getNickname() {
        return nickname;
    };

    public Set<String> getHabits() {
        return habits;
    }

    public void eat(){
        System.out.println("Я їм!!!");
    };

    public abstract void respond();

    public void setSpecies(Species species) {
        this.species = species;
    }

    @Override
    public String toString() {
        return species + "{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                '}';
    }


//    @Override
//    protected void finalize() throws Throwable {
//        super.finalize();
//        System.out.println("Інформація про об'єкт, що видаляється: \n" + this);
//    }
}
