package homeworks_module_01.Homework_08_happy_family_5;

import java.util.Map;

public class Woman extends Human {

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, byte iq, Map<String, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    @Override
    public void greetPet(String typeOfPet) {
        Pet chosenPet = super.chosePetFromList(typeOfPet);
        System.out.printf("Привіт, мілашка %s \n", chosenPet.getNickname());
    }

    public void makeup(){
        System.out.println("Підфарбувалася!");
    }

}
