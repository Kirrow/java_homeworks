package homeworks_module_01.Homework_08_happy_family_5;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyController {
    public FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies(){
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies(){
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int familyBiggerThan){
        return familyService.getFamiliesBiggerThan(familyBiggerThan);
    }

    public List<Family> getFamiliesLessThan(int familyLessThan){
        return familyService.getFamiliesLessThan(familyLessThan);
    }

    public int countFamiliesWithMemberNumber(int countOfPeopleInFamily){
        return familyService.countFamiliesWithMemberNumber(countOfPeopleInFamily);
    }

    public void createNewFamily(Human father, Human mother){
        familyService.createNewFamily(father, mother);
    }

    public void deleteFamilyByIndex(int indexOfFamily){
        familyService.deleteFamilyByIndex(indexOfFamily);
    }

    public Family bornChild(Family family, String maleName, String femaleName){
        return familyService.bornChild(family, maleName, femaleName);
    }

    public Family adoptChild(Family family, Human childToAdoption){
        return familyService.adoptChild(family, childToAdoption);
    }

    public void deleteAllChildrenOlderThen(int age){
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count(){
        return familyService.count();
    }

    Family getFamilyById(int familyIndex){
        return familyService.getFamilyById(familyIndex);
    }

    Set<Pet> getPets(int familyIndex){
        return familyService.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet newPet){
        familyService.addPet(familyIndex, newPet);
    }
}
