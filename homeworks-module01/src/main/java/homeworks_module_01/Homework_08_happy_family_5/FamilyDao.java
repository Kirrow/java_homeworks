package homeworks_module_01.Homework_08_happy_family_5;

import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    Boolean deleteFamily(int index);
    Boolean deleteFamily(Family family);
    void saveFamily(Family family);


}
