package homeworks_module_01.Homework_08_happy_family_5;

import java.util.Set;

public class Fish extends Pet {
    public Species species = Species.FISH;

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        super.setSpecies(Species.FISH);
    };
    public void respond(){
        System.out.printf("Рибка не вміє говорити! \n");
    };



}
