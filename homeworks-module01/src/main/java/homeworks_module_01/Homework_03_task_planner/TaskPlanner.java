package homeworks_module_01.Homework_03_task_planner;

import java.util.Scanner;

public class TaskPlanner {

    public static String[][] schedule = new String[][] {
            {"Sunday", "do home work"},
            {"Monday", "go to courses; watch a film"},
            {"Tuesday", "read a book"},
            {"Wednesday", "go for a walk"},
            {"Thursday", "wash dishes"},
            {"Friday", "clean the bathroom"},
            {"Saturday", "go to gym"},
    };

    static Scanner in = new Scanner(System.in);
    public static void main(String[] args) {

        while (true) {
            printMessage("Please, input the day of the week: ", false);
            String dayOfWeek = in.nextLine().trim().toLowerCase();

            if (dayOfWeek.equals("exit")) {
                printMessage("Goodbye!", false);
                break;
            }

            int dayIndex = getDayIndex(dayOfWeek);

            if (dayOfWeek.startsWith("change") || dayOfWeek.startsWith("reschedule")) {
                int dayIndexForChange = getDayIndex(dayOfWeek.split(" ")[1]);
                if(dayIndexForChange == -1) {
                    printMessage("Please, enter correct day.", true);
                    continue;
                };

                System.out.printf("Please, input new tasks for %s: \n", schedule[dayIndexForChange][0]);
                String newTasks = in.nextLine();
                schedule[dayIndexForChange][1] = newTasks;
                System.out.printf("Tasks for %s updated successfully.\n", schedule[dayIndexForChange][0]);
                continue;
            }

            if (dayIndex == -1) {
                printMessage("Sorry, I don't understand you, please try again.", true);
                continue;
            }

            System.out.printf("Your tasks for %s: %s \n", schedule[dayIndex][0], schedule[dayIndex][1]);

        }

        in.close();
    };

    private static int getDayIndex(String day) {

        for(int i = 0; i < schedule.length; i++){
            if(schedule[i][0].toLowerCase().equals(day.trim().toLowerCase())){
                return i;
            }
        }
        return -1;

    }

    public static void printMessage(String message, boolean isWrapToNextLine){
        System.out.print(message + (isWrapToNextLine ? "\n" : ""));
    };

}
